require 'card.rb'
require 'rspec'

describe Card do
  before(:each) do
    @card = Card.new(5,:heart)
  end

  it "value is right" do
    expect(@card.value).to eq(5)
  end

  it "suit is right" do
    expect(@card.suit).to eq(:heart)
  end


end