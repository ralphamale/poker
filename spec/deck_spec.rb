require 'deck.rb'

describe Deck do
  subject(:deck) { Deck.new }
  it "has 52 cards" do
    expect(deck.deck.count).to eq(52)
  end

  it "has no duplicate cards" do
    expect(deck.deck.uniq).to eq(deck.deck)
  end

  it "shuffles" do
    #this produces false positives..how to reword???
    expect(deck).to_not eq(deck.shuffle)
  end



end