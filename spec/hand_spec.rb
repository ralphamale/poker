require 'hand.rb'
require 'rspec'
require 'card.rb'

describe Hand do


  describe "Initializes right" do

    let (:queenhearts) { double("fivehearts", value: 12, :suit => :heart) }
    let (:acediamonds) { double("acediamonds", value: 14, :suit => :diamond) }
    let (:kingspades) { double("kingspades", value: 13, :suit => :spade)}
    let (:jackclubs) { double("jackclubs", value: 11, :suit => :club)}
    let (:tenhearts) {double("tenDICKhearts", value: 10, :suit => :heart)}

    let (:acehearts) {double("acehearts", value: 14, :suit => :heart)}
    subject (:hand) { Hand.new([queenhearts, acediamonds, kingspades, jackclubs, tenhearts]) }

    it "has five cards" do
      expect(hand.hand.count).to eq(5)
    end

    it "has a card we dealt" do
      expect(hand.hand.include?(tenhearts)).to eq(true)
    end

    it "doesnt have a card we didnt deal" do
      expect(hand.hand.include?(acehearts)).to eq(false)
    end

  end

  describe "Detects singular hands" do
    subject (:hand) { Hand.new([c1, c2, c3, c4, c5])}
    let (:c1) { Card.new(14,:heart) }
    let (:c2) { Card.new(13,:heart) }
    let (:c3) { Card.new(12,:heart) }
    let (:c4) { Card.new(11,:heart) }
    let (:c5) { Card.new(10,:heart) }
    let (:c6) { Card.new(2,:diamond) }
    let (:c6a) {Card.new(2,:club) }
    let (:c7) { Card.new(2,:heart) }
    let (:c7a) { Card.new(2,:spade) }
    let (:c8) { Card.new(5,:diamond) }
    let (:c9) { Card.new(5,:spade) }
    let (:c10) { Card.new(5,:heart) }

    it "detects royal flush" do
      hand = Hand.new([c1,c2,c3,c4,c5])
      expect(hand.royal_flush?).to eq(true)
      expect(hand.best_cards).to eq([c1,c2,c3,c4,c5])
    end
        #
    it "detects full house" do
      hand = Hand.new([c6,c7,c8,c9,c10])
      expect(hand.full_house?).to eq(true)
      expect(hand.best_cards).to eq([c6,c7,c8,c9,c10])
    end

    it "detects four of a kind" do
      hand = Hand.new([c6, c6a, c7, c7a, c8])
      expect(hand.four_of_a_kind?).to eq(true)
      expect(hand.best_cards).to eq([c6,c6a,c7,c7a])
    end


    it "detects straight flush" do
      hand = Hand.new([c1,c2,c3,c4,c5])
      expect(hand.straight_flush?).to eq(true)
      expect(hand.best_cards).to eq([c1,c2,c3,c4,c5])
    end

    it "detects flush" do
      hand = Hand.new([c1,c2,c3,c4,c5])
      expect(hand.flush?).to eq(true)
      expect(hand.best_cards).to eq([c1,c2,c3,c4,c5])

    end
    it "detects straight" do
      hand = Hand.new([c5, c4, c3, c2, c1])
      expect(hand.straight?).to eq(true)
      expect(hand.best_cards).to eq([c5, c4, c3, c2, c1])

    end

    it "detects three of a kind" do
      hand = Hand.new([c8, c9, c10, c5, c1])
      expect(hand.three_of_a_kind?).to eq(true)
      expect(hand.best_cards).to eq([c8, c9, c10])

    end


    it "detects two pair" do
      hand = Hand.new([c9, c8, c3, c6, c7])
      expect(hand.two_pair?).to eq(true)
      expect(hand.best_cards).to eq([c9, c8, c6, c7])

    end

    it "detects one pair" do
      hand = Hand.new([c1, c2, c3, c6, c7])
      expect(hand.pair?).to eq(true)
      expect(hand.best_cards).to eq([c6, c7])
    end

  end


  #detects Ace = low straight
  describe "Picks best hand."


end