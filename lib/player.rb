class Player

  attr_accessor :folded, :game, :hand, :chipstack

  def initialize(name, chipstack, game)
    @hand = nil
    @name, @chipstack = name, chipstack
    @folded = false
    @game = game
  end

  def pay_ante(amt)
    @chipstack -= amt
    @game.add_to_pot(amt)
  end

  def receive_cards(n)
    #@hand += cards
    new_cards = @game.deal_cards(n) # []
    if n = 5
      @hand  = Hand.new(new_cards)

  end

  def user_input
    #F? R? C? S?
  end

  def discard_redraw(cards_to_discard)
    num_of_redraws = cards_to_discard.count

    discarded_cards = []
    cards_to_discard.each do |i|
      discarded_cards << @hand[i]
    end

    @hand.remove(discarded_cards)
    @hand.hand.reject! {|card| discarded_card.include?(card)}

    receive_cards(num_of_redraws)
  end

  def raise(amt)
    @chipstack -= amt
    @game.add_to_pot(amt)
  end

  def fold
    @folded = true
  end

  def call(amt)
  @chipstack -= amt
  @game.add_to_pot(amt)
  end

  def win_pot(amt)
    @chipstack += amt
  end




end