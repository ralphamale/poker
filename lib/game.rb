class Game

  attr_reader :players

  def initialize(ante = 20, *players)
    @players = players
    @deck = Deck.new
    @pot = 0
    @ante = ante
  end


  def play_game
    round_counter = 0
    until game_over?
      dealer_pos = game_counter % @players.count

      unfold_players

      play_hand(dealer_pos)
      # each_player { pay_ante(500) }
      round_counter += 1
    end
  end

  def unfold_players
    @players.each do |player|
      player.folded = false
    end
  end

  def ante_up
    @players.each do |player|
      player.pay_ante(@ante)
      @pot += @ante
    end
  end

  def play_hand(dealer_pos)

    @pot = 0
    ante_up
    #Folded_players = []
    #deal cards

    #round of betting.
    #variable = who raised?
    #variable - minimum pot to stay in game.
    #next if player.folded

    #draw new cards

    #round of betting

    #calculate_winner



  end

  def add_to_pot(amt)
    @pot += amt
  end

  def deal_cards(n)
    @deck.take(n)
  end

end

