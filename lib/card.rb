class Card
  attr_reader :value, :suit

  def initialize(value, suit)
    @value = value
    @suit = suit
  end

  def same_suit?(other_card)
    self.suit == other_card.suit
  end

  def same_value?(other_card)
    self.value == other_card.value
  end

end