require_relative 'card.rb'
require 'debugger'
class Hand
  attr_accessor :hand
  attr_reader :best_cards
  #
  # RANKS = [royal_flush?, straight_flush?,
  #         four_of_a_kind?, full_house?, flush?,
  #         straight?, three_of_a_kind?, two_pair?,
  #         pair?]

  def initialize(cards = nil)
    @best_cards = []
    @hand = cards
  end

  def self.pick_winner(*hands)
    winning_hand = nil
    RANKS.each_index do |i|
      winning_hand = hands.select{ |hand| hand.RANKS[i] }
      return

  end

  def freq_count
    @value_count = Hash.new(0)     #{val => count, val => count}


    @hand.each do |card|
      @value_count[card.value] += 1

    end

  end


  def royal_flush?
    flush? && @hand.all? {|card| card.value >= 10}
  end

  def straight_flush?
    straight? && flush?
  end

  def four_of_a_kind?
    #val_count = Hash.new(0)

    @hand.each do |card|
      same_cards = @hand.select {|card2| card2.value == card.value}
      if same_cards.count == 4
        @best_cards = same_cards
        return true
      end
    end
    return false
  end

  def full_house?
    if @hand.uniq {|card| card.value}.count == 2
      @best_cards = @hand
      return true
    end
    return false
  end

  def flush?
    suit_count = Hash.new(0)     #{suit => count, suit => count}

    hand.each do |card|
      suit_count[card.suit] += 1
    end

    if suit_count.any? {|k,v| v == 5}
      @best_cards = @hand
      return true
    end

    return false
  end

  def straight?
    sorted_vals = @hand.map {|card| card.value}.sort


    5.times do |i|
      return false unless sorted_vals.include?(sorted_vals[0] + i)
    end

    @best_cards = @hand
    return true
  end

  def three_of_a_kind?

    uniq_cards = @hand.uniq { |card| card.value}
    duped_cards = @hand - uniq_cards

    if duped_cards.count == 2 && duped_cards.uniq { |card| card.value}.count == 1
      @best_cards = @hand.select {|card| card.value == duped_cards[0].value}
      return true
    end
    return false
  end

  def two_pair?
    pairs = []
    @hand.each_index do |i|
      (i..4).each do |j|
        next if i == j
        #hold an array.
        if @hand[i].same_value?(@hand[j])
          pairs += [@hand[i], @hand[j]]
        end
      end
    end
    if pairs.count == 4
      @best_cards = pairs
      return true
    end

    return false
  end


  def pair?
    @hand.each_index do |i|
      @hand.each_index do |j|
        next if i == j
        #hold an array.
        if @hand[i].same_value?(@hand[j])
          @best_cards = [@hand[i], @hand[j]]
          return true
        end
      end
    end
    return false
  end


end
