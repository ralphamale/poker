require_relative 'card.rb'

class Deck
  SUITS = [:heart, :spade, :diamond, :club]

  attr_accessor :deck
  def initialize
    @deck = generate_cards
    shuffle
  end

  def generate_cards
    deck = []

    SUITS.each do |suit|
      (1..13).each do |value|
       deck << Card.new(value, suit)
      end
    end

    deck
  end

  def shuffle
    @deck.shuffle!
  end

  def take(n)
    @deck.pop(n)
  end

end